package repository

import (
	"github.com/jmoiron/sqlx"
	"log"
	"testSQLX/models"
)

type AuthPostgres struct {
	db *sqlx.DB
}

func NewAuthPostgres(db *sqlx.DB) *AuthPostgres {
	return &AuthPostgres{db: db}
}

func (r *AuthPostgres) CheckSession(identifier string) (*models.Account, error) {
	var account models.Account

	if err := r.db.Get(&account, `select login, password, credits from "Account"
	    where login=$1`,
		identifier); err != nil {
		return nil, err
	}
	log.Println("password")
	log.Println(account.Password == "")

	//if err := pgxscan.NewScanner(r.db.QueryRow(`select password from "Account"
	// where login=$1`,
	//	identifier)).Scan(&account.Password); err != nil {
	//	return nil, err
	//}

	//rows := r.db.QueryRow(`select password from "Account"
	// where login=$1`,
	//	identifier)
	//pgxscan.ScanAll(&account, rows)
	//log.Println(account)
	//ctx := context.Background()
	//if err := pgxscan.Get(
	//	ctx, r.db, &account, `select login, password, credits from "Account"
	//	where login=$1`,
	//	identifier); err != nil {
	//	return nil, err
	//}

	return &account, nil
}
