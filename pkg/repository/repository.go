package repository

import (
	"github.com/jmoiron/sqlx"
	"testSQLX/models"
)

type Auth interface {
	CheckSession(identifier string) (*models.Account, error)
}

type Repository struct {
	Auth Auth
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Auth: NewAuthPostgres(db),
	}
}
