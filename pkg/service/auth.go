package service

import (
	"testSQLX/models"
	"testSQLX/pkg/repository"
)

type AuthService struct {
	repo repository.Auth
}

func NewAuthService(repo Auth) *AuthService {
	return &AuthService{repo: repo}
}

func (s *AuthService) CheckSession(identifier string) (*models.Account, error) {
	return s.repo.CheckSession(identifier)
}
