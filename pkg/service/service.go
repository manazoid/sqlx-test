package service

import (
	"testSQLX/models"
	"testSQLX/pkg/repository"
)

type Auth interface {
	CheckSession(identifier string) (*models.Account, error)
}

type Service struct {
	Auth Auth
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Auth: NewAuthService(repos.Auth),
	}
}
