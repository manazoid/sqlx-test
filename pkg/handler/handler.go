package handler

import (
	"github.com/gin-gonic/gin"
	"testSQLX/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) GetRouter() *gin.Engine {
	router := gin.Default()

	router.GET("/:id", h.CheckSession)

	return router
}
