package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *Handler) CheckSession(c *gin.Context) {
	rowID := c.Param("id")
	if rowID == "" {
		c.Status(http.StatusBadRequest)
		return
	}

	account, err := h.services.Auth.CheckSession(rowID)
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, account)
}
