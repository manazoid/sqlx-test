package models

type Account struct {
	Login    string `json:"login" db:"login"`
	Password string `json:"-" db:"password"`
	Credits  int    `json:"credits" db:"credits"`
}
