-- auto-generated definition
create table "Account"
(
    id            varchar                                            not null
        constraint account_pk
            primary key,
    login         varchar                                            not null,
    password      varchar                                            not null,
    credits       integer                  default 0                 not null,
    create_date   timestamp with time zone default CURRENT_TIMESTAMP not null,
    delete_date   timestamp with time zone,
    telegram_data jsonb
);

alter table "Account"
    owner to postgres;

create unique index account_login_uindex
    on "Account" (login);

