package main

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"testSQLX/pkg/handler"
	"testSQLX/pkg/repository"
	"testSQLX/pkg/service"
	"time"
)

type (
	Server struct {
		httpServer *http.Server
	}

	Config struct {
		Host     string
		Port     string
		Username string
		Password string
		DBName   string
		SSLMode  string
	}
)

func main() {
	serverInstance := new(Server)
	postgresConfig := Config{
		Host:     "localhost",
		Port:     "5432",
		Username: "postgres",
		Password: "1234",
		DBName:   "promocode",
		SSLMode:  "disable",
	}

	ctxTimeout, ctxCancel := context.WithTimeout(context.Background(), time.Second*3)
	defer ctxCancel()

	db, err := sqlx.ConnectContext(ctxTimeout, "postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		postgresConfig.Host, postgresConfig.Port, postgresConfig.Username, postgresConfig.Password, postgresConfig.DBName, postgresConfig.SSLMode))
	if err != nil {
		panic(err)
	}

	db.SetMaxIdleConns(5)
	db.SetConnMaxIdleTime(10 * time.Second)
	db.SetMaxOpenConns(95)

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	runServer(serverInstance, handlers)
}

func runServer(serverInstance *Server, handlerLayer *handler.Handler) {
	port := "8080"
	router := handlerLayer.GetRouter()

	if err := serverInstance.Run(port, router); err != nil {
		log.Fatal(err)
	}

	log.Print("server started successfully")
}
