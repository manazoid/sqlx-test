package main

import (
	"net/http"
	"time"
)

func (s *Server) Run(port string, router http.Handler) error {
	s.httpServer = &http.Server{
		Handler:        router,
		Addr:           ":" + port,
		MaxHeaderBytes: 1 << 20, // 1MB
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}
	return s.httpServer.ListenAndServe()
}
